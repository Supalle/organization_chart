let base = {
    /**
     * 从树形数据中按层级取数据
     * @param {Array} list 树形数据
     * @param {String} childKey 子节点字段
     * @param {Number} layer 目标成绩
     * @param {Number} currentLayer 当前层级
     */
    layerTree(list, childKey, layer, currentLayer) {
        if (list && Array.isArray(list) && list.length > 0) {
            list.forEach(item => {
                if (currentLayer < layer) {
                    this.layerTree(item[childKey], childKey, layer, currentLayer + 1)
                }else{
                    delete item[childKey]
                }
            });
        }
    },

    /**
     * 深拷贝,返回拷贝后的对象
     * @param {Object} target  要深拷贝的对象
     */
    deepClone (target) {
        let result, targetType = this.getObjClass(target);   
        if(targetType === 'Object'){
            result = {};
        }else if(targetType === 'Array'){
            result = [];
        }else {
            return target;
        }

        for(let key in target){
            let value = target[key];
            // eslint-disable-next-line no-constant-condition
            if(this.getObjClass(value) === "Object" || 'Array'){
                result[key] = this.deepClone(value);
            }else {
                result[key] = target[key];
            }
        }
        return result;
    },

    //检测数据类型
    getObjClass(obj) {
        return Object.prototype.toString.call(obj).slice(8, -1);
    },
};

export default base