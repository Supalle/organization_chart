# 组织架构图

#### 介绍
html+js实现组织架构图 

#### 软件架构
1.  vue.js
2.  element-ui
3.  OrgChart JS地址：https://balkangraph.com/


#### 安装教程

1.  cnpm i

#### 使用说明

1.  npm run serve

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  我的CSDN博客：https://blog.csdn.net/littlebearGreat

![输入图片说明](https://images.gitee.com/uploads/images/2020/0319/190534_9690519b_1816492.png "组织架构图.png")
