// 机构图
import OrgChart from '@balkangraph/orgchart.js/orgchart';
export default class Chart {
    direction   = 'top'                 // 图形方向： top-从上到下，left-从左到右
    idKey       = 'orgId'               // 节点id key
    parentIdKey = 'orgParentId'         // 节点的父id key
    childKey    = 'childList'           // 子节点key

    // 初始化
    init(dom, treeData) {
        let tree = this.delParentId(treeData);
        let list = [];
        this.treeArray(tree, list);
        return this.draw(dom, list);
    }

    // 删除顶级节点的父id
    delParentId(treeData) {
        if (!treeData) {
            console.error('机构图数据为空，请检查')
            return false; 
        }
        if (!Array.isArray(treeData)) {
            console.error('机构图数据必须是数组，请检查')
            return false; 
        }
        if (treeData.length === 0) {
            return false;
        }

        let list = treeData.map(item => {
            delete item[this.parentIdKey];
            return item;
        })
        return list;
    }

    // 把树形格式化为数组
    treeArray(list, result = []) {
        if (list && Array.isArray(list) && list.length > 0) {
            list.forEach(item => {
                item.id = item[this.idKey];
                item.pid = item[this.parentIdKey];
                item.name = `${item.orgManagerName}/${item.orgManagerPositionName}`;
                item.number = `${item.staffNumbers}/${item.planNumbers}`;
                item.img = require('../img/user.png');
                result.push(item);
                this.treeArray(item[this.childKey], result);
            });
        }
    }

    // 渲染机构树图形
    draw(dom, list){
        this.setTmp();
        let nodes = list.map(item => {
            item.tags = ["Management"];
            return item;
        })
        return new OrgChart(dom, {
            nodes: nodes,
            nodeBinding: {
                field_0: "orgName",
                field_1: "name",
                field_2: "number",
                img_0: "img"
            },
            tags: {
                Management: {
                    template: "myTemplate"
                }
            },
            // menu: {
            //     pdf: { text: "导出 PDF" },
            //     png: { text: "导出 PNG" },
            //     svg: { text: "导出 SVG" },
            //     csv: { text: "导出 CSV" }
            // },
            toolbar: {
                layout: false,
                zoom: true,
                fit: true,
                expandAll: false
            },
            orientation: OrgChart.orientation[this.direction],          // 方向
            enableSearch: false,                                        // 是否可以搜索
            nodeMouseClick: OrgChart.action.none,                       // 关闭点击后出现的效果，该效果有很多，可以去官网查看
        });
    }

    // 模板
    setTmp() {
        OrgChart.templates.myTemplate = Object.assign({}, OrgChart.templates.ana);
        OrgChart.templates.myTemplate.size = [168, 80];
        // OrgChart.templates.myTemplate.node = '<circle cx="100" cy="100" r="100" fill="#fff" stroke-width="1" stroke="#1C1C1C"></circle>';
        // OrgChart.templates.myTemplate.node = '<rect class="hy_node" width="168" height="80" rx="4" ry="4" fill="#fff" ></rect>';
        OrgChart.templates.myTemplate.node = 
        `<g>
            <rect class="hy_node" width="168" height="80" x="0" y="0" rx="4" ry="4" fill="#ccc" filter="url(#f1)" ></rect>
            <filter id="f1" x="0" y="0">
                <feGaussianBlur in="SourceGraphic" stdDeviation="4" />
            </filter>
            <rect class="hy_node" width="168" height="80" rx="4" ry="4" fill="#fff" ></rect>
        </g>
        `;

        // 鼠标点击效果
        OrgChart.templates.myTemplate.ripple = {
            radius: 0,
            color: "#FC8E58",
            rect: null
        };

        OrgChart.templates.myTemplate.field_0 = 
        `<g>
            <rect class="hy_node" width="168" height="32" rx="4" ry="4" fill="#FC8E58" ></rect>
            <rect class="hy_node" width="168" y="12" height="20" fill="#FC8E58" ></rect>
            <text style="font-size: 14px;" fill="#fff" x="84" y="21" text-anchor="middle">{val}</text>
        </g>
        `;
        OrgChart.templates.myTemplate.field_1 = '<text style="font-size: 12px;" width="102" text-overflow="ellipsis" fill="#676B6D" x="52" y="52" text-anchor="left">{val}</text>';
        OrgChart.templates.myTemplate.field_2 = '<text style="font-size: 12px;" width="102" text-overflow="ellipsis" fill="#FF8C58" x="52" y="70" text-anchor="left">{val}</text>';

        OrgChart.templates.myTemplate.img_0 = 
              '<clipPath id="ulaImg">'
            + '<rect class="hy_node_img" x="8" y="40" width="32" height="32" fill="#ccc" ></rect>'
            + '</clipPath>' 
            + '<image preserveAspectRatio="xMidYMid slice" clip-path="url(#ulaImg)" xlink:href="{val}" x="8" y="40" width="32" height="32">'
            + '</image>';

        // 连接线条
        // OrgChart.templates.myTemplate.link = '<path stroke="#686868" stroke-width="1px" fill="none" link-id="[{id}][{child-id}]" d="M{xa},{ya} C{xb},{yb} {xc},{yc} {xd},{yd}" />';

        // OrgChart.templates.myTemplate.nodeMenuButton = 
        //     '<g style="cursor:pointer;" transform="matrix(1,0,0,1,93,15)" control-node-menu-id="{id}">'
        //     + '<rect x="-4" y="-10" fill="#000000" fill-opacity="0" width="22" height="22">'
        //     + '</rect>'
        //     + '<line x1="0" y1="0" x2="0" y2="10" stroke-width="2" stroke="#0890D3" />'
        //     + '<line x1="7" y1="0" x2="7" y2="10" stroke-width="2" stroke="#0890D3" />'
        //     + '<line x1="14" y1="0" x2="14" y2="10" stroke-width="2" stroke="#0890D3" />'
        //     + '</g>';

        OrgChart.templates.myTemplate.plus = 
            '<rect x="6" y="7" width="16" height="16" rx="8" ry="8" fill="#fff" stroke="#aeaeae" stroke-width="1"></rect>'
            + '<line x1="10" y1="15" x2="18" y2="15" stroke-width="1" stroke="#aeaeae"></line>'
            + '<line x1="14" y1="11" x2="14" y2="19" stroke-width="1" stroke="#aeaeae"></line>'

        OrgChart.templates.myTemplate.minus = 
            '<rect x="6" y="7" width="16" height="16" rx="8" ry="8" fill="#fff" stroke="#aeaeae" stroke-width="1"></rect>'
            + '<line x1="10" y1="15" x2="18" y2="15" stroke-width="1" stroke="#aeaeae"></line>'

        // OrgChart.templates.myTemplate.expandCollapseSize = 36;

        // OrgChart.templates.myTemplate.exportMenuButton = 
        //     '<div style="position:absolute;right:{p}px;top:{p}px; width:40px;height:50px;cursor:pointer;" control-export-menu="">'
        //     + '<hr style="background-color: #0890D3; height: 3px; border: none;">'
        //     + '<hr style="background-color: #0890D3; height: 3px; border: none;">'
        //     + '<hr style="background-color: #0890D3; height: 3px; border: none;">'
        //     + '</div>';  

        // OrgChart.templates.myTemplate.pointer = 
        //     '<g data-pointer="pointer" transform="matrix(0,0,0,0,100,100)">><g transform="matrix(0.3,0,0,0.3,-17,-17)">'
        //     + '<polygon fill="#0890D3" points="53.004,173.004 53.004,66.996 0,120" />'
        //     + '<polygon fill="#0890D3" points="186.996,66.996 186.996,173.004 240,120" />'
        //     + '<polygon fill="#0890D3" points="66.996,53.004 173.004,53.004 120,0" />'
        //     + '<polygon fill="#0890D3" points="120,240 173.004,186.996 66.996,186.996" />'
        //     + '<circle fill="#0890D3" cx="120" cy="120" r="30" />'
        //     + '</g></g>';
    }

    constructor(dom, treeData ,direction = 'top') {
        this.direction = direction;
        return this.init(dom, treeData);
    }
}